.. _trs-recipes:

TRS recipes
###########
TRS levarage various layers and recipes to build firmware, root filesystem and
various images. The best place to start looking for the recipes used, would be
in the manifest files (``*.xml``) in trs-manifest.git_.

.. _trs-manifest.git: https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest
