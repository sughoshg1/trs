.. _getting_started:

Getting started
###############
The instructions mentioned on this page is a one time setup (per workspace).

1. Install repo
***************
Note that here you don’t install a huge SDK, it’s simply a Python script that
you download and put in your ``$PATH``, that’s it. Exactly how to “install”
repo, can be found at the Google `repo`_ pages, so follow those instructions
before continuing.

2. Getting the source code
**************************
Now we will check out code for the TRS. This step is light weight and only check
out code necessary to build TRS. There are two flavors right now, either you
checkout the one tracking lastest on all gits or you'll checkout a certain
release (the difference is in the ``repo init`` line, as highlighted).

For latest, do this
===================

.. code-block:: bash
   :emphasize-lines: 3

    $ mkdir trs-workspace
    $ cd trs-workspace
    $ repo init -u https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest.git -m default.xml
    $ repo sync -j3

For a specific release, do this
===============================

.. code-block:: bash
   :emphasize-lines: 3

    $ mkdir trs-workspace
    $ cd trs-workspace
    $ repo init -u https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest.git -m default.xml -b <release-tag>
    $ repo sync -j3

.. _prerequisites:

3. Installing prerequisites
***************************
TRS depends on a couple of packages that needs to be present on the host system.
These are installed as distro packages and using Python pip.

Host packages
=============
.. tabs::

    .. tab:: Ubuntu / Debian

        **Host / apt packages**

        This will require your **sudo** password, from the root of the
        workspace:

        .. code-block:: bash

            $ cd <workspace root>
            $ make apt-prereqs

        This will install the following packages:

        .. literalinclude:: ../../scripts/apt-packages.txt
            :language: text


    .. tab:: Arch Linux

        .. warning::
            Just boiler plate, no complete instructions. Only Ubuntu versions
            tested so far.

        Install the necessary packages using pacman.

        ..
          [NEEDS_TO_BE_FIXED] - Add correct pre-req packages

        .. code-block:: bash

            $ sudo pacman -Syy
            $ sudo pacman -S git

    .. tab:: Fedora

        .. warning::
            Just boiler plate, no complete instructions. Only Ubuntu versions
            tested so far.

        Install the necessary packages using dnf.

        ..
          [NEEDS_TO_BE_FIXED] - Add correct pre-req packages

        .. code-block:: bash

            $ sudo dnf update
            $ sudo dnf install git

Python packages
===============

By default all python packages will be installed at ``<workspace
root>/.pyvenv`` using a virtual Python enviroment. The benefits by doing
so is that if we delete the ``.pyvenv`` folder, there will be no traces
left of the Python packages needed for TRS. It can eventually also avoid
clashing with tools needing other versions of some Python packages.

.. code-block:: bash

    $ cd <workspace root>
    $ make python-prereqs

4. Building
***********

4.1 Support virtualization with Xen (Optional)
==============================================

To support Xen in TRS, in the configuration file ``meta-trs/conf/distro/trs.conf``
you need to replace distro feature ``ewaol-baremetal`` with ``ewaol-virtualization``
and append to variable ``DISTRO_FEATURES``; with the virtualization feature, Xen
hypervisor and its associated packages (including kernel modules and tools) will
be built in TRS image.

.. code-block:: bash

    # In the file meta-trs/conf/distro/trs.conf
    DISTRO_FEATURES:append = " ewaol-virtualization"

4.2 Build firmwares and TRS image
=================================

Since we are using a virtual Python environment, we need begin by sourceing it.

.. code-block:: bash

    $ source <workspace root>/.pyvenv/bin/activate

.. note::
   The ``source`` command must be run **once** each time a new shell is created.

Next we start the build, this will probably take several hours on a normal
desktop computer the first time you're building it with nothing in the cache(s).
The TRS is based on various Yocto layers and if you don't have your ``DL_DIR``
and ``SSTATE_DIR`` set as an environment variable, those will be set to
``$HOME/yocto_cache`` by default. Note that the ``clean`` target does not remove
the download and sstate caches. ``make clean`` is a rather quick process that is
often needed after modifying the Yocto meta layers.

.. code-block:: bash

    $ cd <workspace root>
    $ make

After you complete the whole building process, if you want to only build
firmwares for saving time, you could use the command ``make meta-ts``; for only
building TRS image, the command ``make trs`` can be used.  You also can use the
command ``make trs-dev``, it builds TRS image with enabling `ewaol-sdk` distro
feature and includes debugging and profiling tools (e.g. gdb, perf, systemtap,
ltt-ng, etc).

5. Target specific installation
*******************************
After following the steps above, please continue with the target specific
instructions:

#. :ref:`install-qemu`
#. :ref:`install-other`

5. Tips and tricks
******************

5.1 Reference local mirrors
===========================
As the repo forest grows, the amount of time to run the initial ``repo sync``
increases. The repository tool is able to reference a locally cloned forest and
clone the bulk of the code from there, taking just the eventual delta between
local mirrors and upstream trees. The way to do this is to add the parameter
``--reference`` when running the ``repo init`` command, for example:

.. code-block:: bash

    $ repo init -u https://... --reference <path-to-my-existing-forest>

5.2 Local manifests
===================
In some cases we might want to use another remote, pick a certain commit or even
a add another repository to the current repo setup. The way to do that with
``repo`` is to use `local manifests`_. The end result would be the same as
manually clone or checkout a certain tag or commit. The advantage of using a
local manifest is that when running "repo sync", the original manifest will not
override our temporary modifications. I.e., it's possible to reference and keep
using a temporary copy if needed.

.. _repo: https://source.android.com/docs/setup/develop#installing-repo
.. _local manifests: https://gerrit.googlesource.com/git-repo/+/master/docs/manifest-format.md#Local-Manifests
