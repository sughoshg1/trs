.. _install-qemu:

Install QEMU
############

This document describes how to run TRS for the QEMU target. It is assumed
that you have completed the procedures outlined on the :ref:`getting started`
page. If not, begin there before proceeding.

Run
***
After the build is complete, you will be able to run it on your host system
using QEMU.

.. code-block:: bash

    $ make run

U-Boot is already set to boot the current kernel, initramfs, and rootfs upon
initial startup.

.. note::
    To quit QEMU, press ``Ctrl-A x`` (alternatively kill the qemu-system-aarch64 process)

If everything goes as planned, you will be greeted with a login message and a
login prompt. The login name is ``ewaol`` as depicted below.

.. code-block:: bash

    ledge-secure-qemuarm64 login: ewaol
    ewaol@ledge-secure-qemuarm64:~$

Alternatively if you want to launch QEMU manually follow the instructions :ref:`Run on QEMU arm64`

Test
****
Once the build has been completed, you can run automatic tests with QEMU. These
boot QEMU using the compiled images and run test commands via SSH on the running
system. While the QEMU image is running, SSH access to it works via localhost IP
address ``127.0.0.1`` and TCP port ``2222``. ``TEST_SUITES`` variable in
``trs-image.bb`` recipe define which tests are executed.

.. code-block:: bash

    $ cd <workspace root>
    $ make test

See `Yocto runtime testing documentation`_ for details about the test
environment and `instructions for writing new tests`_.

.. _instructions for writing new tests: https://docs.yoctoproject.org/singleindex.html#writing-new-tests
.. _Yocto runtime testing documentation: https://docs.yoctoproject.org/singleindex.html#performing-automated-runtime-testing
