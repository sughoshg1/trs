TRS - Trusted Reference Stack by Linaro
#######################################

The main documentation for the site is organized into a couple sections:

* :ref:`about-docs`
* :ref:`install-docs`
* :ref:`security-docs`
* :ref:`feature-docs`

.. _about-docs:

.. toctree::
	:maxdepth: 2
	:caption: About

	about/about

.. _install-docs:

.. toctree::
	:maxdepth: 2
	:caption: Installation

	install/install
	install/bare_metal
	install/qemu
        install/recipes
	install/faq

.. _firmware-docs:

.. toctree::
	:maxdepth: 2
	:caption: Firmware

	firmware/index

.. _feature-docs:

.. toctree::
	:maxdepth: 2
	:caption: Features

	features/feature

.. _security-docs:

.. toctree::
	:maxdepth: 2
	:caption: Security

	security/security
