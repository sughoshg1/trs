# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Require ewaol image recipes at the very beginning place so avoid
# ewaol recipes to override varibles.
require trs-image-ewaol.inc

IMAGE_FSTYPES = "wic wic.bz2 wic.qcow2"

SUMMARY = "TRS image"
LICENSE = "MIT"

SRC_URI = "file://testimage_data.json"
DEPENDS += "e2fsprogs-native swtpm-native"

inherit features_check deploy

# Enable EFI booting configurations in meta-ledge-secure layer
inherit image-efi-boot

# re-enable SRC_URI handling, it's disabled in image.bbclass
python __anonymous() {
    d.delVarFlag("do_fetch", "noexec")
    d.delVarFlag("do_unpack", "noexec")
}

IMAGE_FEATURES:append = " debug-tweaks"

# setup testimage.bbclass to execute oeqa runtime tests
# TODO: add systemd test once boot is clean
TEST_RUNQEMUPARAMS = "slirp nographic novga"

# keep ping and ssh first since most tests depend on them
# and this variable used for test execution order too
# https://docs.yoctoproject.org/singleindex.html#term-TEST_SUITES
TEST_SUITES = "\
    ping \
    ssh \
    tpm \
    opteetest \
    date \
    df \
    parselogs \
    ptest \
    python \
    rtc \
    soafeetestsuite \
    systemd \
    tpm \
"

# uses meta-ledge-secure secure boot
IMAGE_INSTALL += "\
    ${@bb.utils.contains("DISTRO_FEATURES", "wifi", "packagegroup-base-wifi", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "optee", "packagegroup-ledge-optee", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "tpm2", "packagegroup-ledge-tpm-lava", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "tpm2", "packagegroup-security-tpm2", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "tsn", "packagegroup-ledge-tsn", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "grub", "packagegroup-ledge-grub", "", d)} \
    efivar \
    fwts \
    fwupd \
    fwupd-efi \
    kernel-modules \
    kmod \
    optee-test \
    strace \
    xz \
"

EXTRA_IMAGE_FEATURES += "package-management"

do_deploy() {
    # to customise oeqa tests
    mkdir -p "${DEPLOYDIR}"
    install "${WORKDIR}/testimage_data.json" "${DEPLOYDIR}"
    # workaround meta-ts firmware link for testimage.bbclass
    ( cd "${DEPLOYDIR}" && \
      ln -sf ../../../../tmp_tsqemuarm64-secureboot/deploy/images/tsqemuarm64-secureboot/flash.bin flash.bin \
    )
}
# do_unpack needed to run do_fetch and do_unpack which are disabled by image.bbclass.
addtask deploy before do_build after do_rootfs do_unpack

# test depend on native tools like qemu and swtpm, fixes rebuild with full caching
do_testimage[depends] += "swtpm-native:do_populate_sysroot"
# leave native sysroot around so we can use swtpm from there outside of bitbake builds
RM_WORK_EXCLUDE_ITEMS += "recipe-sysroot-native"

# export tests automatically at image build time
addtask testexport before do_build after do_image_complete
